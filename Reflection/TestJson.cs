using System;
using Newtonsoft.Json;

namespace Reflection
{
    public class TestJson
    {
        private TestClassF _v;
        public TestJson(TestClassF inputClassObject)
        {
            _v = inputClassObject;
        }
        public string SerializeJson()
        { 
            return  JsonConvert.SerializeObject(_v);
        }
        
        public void DeserializeJson(string source)
        { 
            _v = JsonConvert.DeserializeObject<TestClassF>(source);
        }
    }
}