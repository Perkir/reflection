using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

namespace Reflection
{
    public static class TestReflection
    {
        public static TestClassF DeserializeReflection(string source)
        {
            var deserialized = new TestClassF();
            var myType = deserialized.GetType();
            var vfields = source.Split(';');
            var vfield = source.Split(':');
            foreach (var f in vfields)
            {
                vfield = f.Split('=');
                if (vfield[0] == "") continue;
                var field = myType.GetField(vfield[0]);
                Type propType = Type.GetType(vfield[2]);
                if (propType == null) continue;
                TypeConverter typeConverter = TypeDescriptor.GetConverter(propType);
                object propValue = typeConverter.ConvertFromString(vfield[1]);
                field.SetValue(deserialized, propValue);
            }
            PrintFields(deserialized);

            return deserialized;
        }
        public static void PrintFields(TestClassF input)
        {
            Type myType = input.GetType();
            FieldInfo[] fields = myType.GetFields();
            foreach (var field in fields)
            {
                Console.WriteLine($"name: {field.Name} value:{field.GetValue(input)}:{field.FieldType}");
            }
        }

        public static string SerializeReflection(object Class)
        {
            var myType = Class.GetType();
            var fields = myType.GetFields();
            return fields.Aggregate(string.Empty, (current, field) => current + $"{field.Name}={field.GetValue(Class)}" +
                                                                      $"={field.FieldType};");
        }
      
    }
}