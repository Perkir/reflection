﻿using System;
using System.Diagnostics;
using static System.String;

namespace Reflection
{
    class Program
    {
        public static int count { get; set; } = 100_000;

        static void Main(string[] args)
        {
            var testClass = new TestClassF()
            {
                i1 = 5, i2 = Guid.Empty, i3 = "World", i4 = '!', i5 = false, i6 = "Hello"
            };
            //RunReflectionTest(testClass, count);
            //RunJsonTest(testClass, count);
            
            var serializedString = TestReflection.SerializeReflection(testClass);
            TestReflection.PrintFields(testClass);
            var deserializedTest = TestReflection.DeserializeReflection(serializedString);
        }
        public static void RunReflectionTest(TestClassF testClass, int count)
        {
            var mytestStr = Empty;
            Console.WriteLine($"Мой рефлекшен: Сериализация. Количество итераций: {count}");
            var sw = Stopwatch.StartNew();
            for (var i = 0; i < count; i++)
            {
                mytestStr = TestReflection.SerializeReflection(testClass);
            }
            sw.Stop();
            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
            Console.WriteLine("Мой рефлекшен: Десериализация");
            sw = Stopwatch.StartNew();
            for (var j = 0; j < count; j++)
            {
                TestReflection.DeserializeReflection(mytestStr);
            }
            sw.Stop();
            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
        }
        
        public static void RunJsonTest(TestClassF testClass, int count)
        {
            var newtonsoftTest = new TestJson(testClass);
            var newtonsoftTestStr = Empty;
            Console.WriteLine($"NewtonsoftJson: Сериализация. Количество итераций: {count}");
            var sw = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
            {
                newtonsoftTestStr = newtonsoftTest.SerializeJson();
            }
            sw.Stop();
            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
            Console.WriteLine("NewtonsoftJson: Десериализация");
            sw = Stopwatch.StartNew();
            for (var j = 0; j < count; j++)
            {
                newtonsoftTest.DeserializeJson(newtonsoftTestStr);
            }
            sw.Stop();
            Console.WriteLine($"Затрачено Milliseconds: {sw.ElapsedMilliseconds}");
        }
    }
}